import angular from 'angular';
import uiRouter from 'angular-ui-router';
import routing from './main.routes';

export class MainController {
  $http;

  users = [];

  /*@ngInject*/
  constructor($http) {
    this.$http = $http;
  }

  $onInit() {
    this.$http.get('/api/users')
      .then(response => {
        this.users = response.data;
      });
  }

  setUser(firstName, lastName, email, password) {
    this.$http.post('/api/users', {
      firstName: firstName,
      lastName: lastName,
      email: email,
      password: password,
    });
  }
}

export default angular.module('usersApp.main', [uiRouter])
  .config(routing)
  .component('main', {
    template: require('./main.html'),
    controller: MainController
  })
  .name;
