import angular from 'angular';
import uiRouter from 'angular-ui-router';
import routing from './detail.routes';

export class MainController {
  $http;

  users = [];

  /*@ngInject*/
  constructor($http) {
    this.$http = $http;
  }

  $onInit() {
    this.$http.get('/api/users')
      .then(response => {
        this.users = response.data;
      });
  }

  setUser() {
    this.$http.post('/api/users', {
      firstName: 'one',
      lastName: 'two',
      email: 'three@yopmail.com',
      password: 'four',
    });
  }
}

export default angular.module('usersApp.main', [uiRouter])
  .config(routing)
  .component('main', {
    template: require('./main.html'),
    controller: MainController
  })
  .name;
