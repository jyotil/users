'use strict';

export default function routes($stateProvider) {
  'ngInject';

  $stateProvider.state('detail', {
    url: '/',
    template: '<detail></detail>'
  });
}
