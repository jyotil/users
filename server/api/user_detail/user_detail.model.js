'use strict';

import mongoose from 'mongoose';
import {registerEvents} from './user_detail.events';

var UserSchema = new mongoose.Schema({
  firstName: String,
  lastName: String,
  email: String,
  password: String
});

registerEvents(UserSchema);
export default mongoose.model('User', UserSchema);
